### Batalha Naval 2.0


1. Abra o programa no Netbeans;

2. Vá na pasta View e dê RUN no arquivo Main.java

3. Irá aparecer:

**Batalha Naval 2.0 :**

    * Abrir o arquivo e começar o jogo
    * Mostrar as instruções

4. Obs : Os botões da interface principal e de voltar não funcionam, logo para voltar terá que fechar o programa e abrir novamente

5. Apenas os botões : "Abrir o arquivo e começar o jogo" e o "Mostrar as instruções" funcionam
