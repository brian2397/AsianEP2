/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

/**
 *
 * @author brian
 */

import java.io.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import Model.Campo;
import View.InterfaceJogo;
import View.Instrucoes;

public class menuAcoes implements ActionListener {
    private JFileChooser mainFile;
    private JFrame mainFrame;
    private File arquivo;
    private char [][] field;
    Campo dataFile = new Campo();
    Campo temp = new Campo();
    
    public void saidas(JFrame mainFrame, JFileChooser mainFile){
        this.mainFrame = mainFrame;
        this.mainFile = mainFile;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(comando.equals("Abrir")){
            int returnVal = mainFile.showOpenDialog(mainFile);
            if(returnVal == JFileChooser.APPROVE_OPTION){
                arquivo = mainFile.getSelectedFile();
                dataFile.Read(arquivo);    
                InterfaceJogo iniciar = new InterfaceJogo(dataFile.getAltura(), dataFile.getLargura(), dataFile.getData());
            }
        }
        else if(comando.equals("Instrucoes")){
            Instrucoes infor = new Instrucoes();
        }
        else if(comando.equals("Voltar")){
            Menu novoMenu = new Menu();
        }
    }
    
}
