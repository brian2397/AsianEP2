/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author brian
 */

import java.io.*;
import javax.swing.*;
import java.awt.*;

import Controller.menuAcoes;

public class Instrucoes {
    private JFrame mainFrame;
    private JLabel mainLabel;
    private JLabel mainLabel2;
    private JLabel mainLabel3;
    private JLabel mainLabel4;
    private JLabel mainLabel5;
    private JLabel mainLabel6;
    private JLabel mainLabel7;
    private JButton mainButton8;
    
    private menuAcoes Listener;
    
    public Instrucoes(){
        preparaGUI3();
    }
    
    private void preparaGUI3(){
        Listener = new menuAcoes();
        
        mainFrame = new JFrame("Batalha Naval");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(1000,800);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        
        mainLabel = new JLabel("BATALHA NAVAL 2.0");
        mainLabel.setBounds(400, 50, 200, 100);
        
        mainLabel2 = new JLabel();
        mainLabel2.setText("Batalha Naval 2.0 consiste em destruir, com os recursos disponiveis, todos os alvos inimigos em um campo de batalha");
        mainLabel2.setBounds(50, 100, 800, 100);
        
        mainLabel3 = new JLabel();
        mainLabel3.setText("Habilidades:");
        mainLabel3.setBounds(50, 150, 800, 100);
        
        mainLabel4 = new JLabel();
        mainLabel4.setText("Atacar uma posição do tabuleiro");
        mainLabel4.setBounds(150, 200, 800, 100);
        
        mainLabel5 = new JLabel();
        mainLabel5.setText("Descobrir se há uma embarcação inimiga em uma área 2x2 do tabuleiro");
        mainLabel5.setBounds(150, 250, 800, 100);
        
        mainLabel6 = new JLabel();
        mainLabel6.setText("Atacar uma área 2x2 do tabuleiro");
        mainLabel6.setBounds(150, 300, 800, 100);
        
        mainLabel7 = new JLabel();
        mainLabel7.setText("Atacar uma linha/coluna completa do tabuleiro");
        mainLabel7.setBounds(150, 350, 800, 100);
        
        mainButton8 = new JButton("Voltar");
        mainButton8.setBounds(800,600,100,50);
        mainButton8.setActionCommand("Voltar");
        mainButton8.addActionListener(Listener);
        
        mainFrame.add(mainLabel);
        mainFrame.add(mainLabel2);
        mainFrame.add(mainLabel3);
        mainFrame.add(mainLabel4);
        mainFrame.add(mainLabel5);
        mainFrame.add(mainLabel6);
        mainFrame.add(mainLabel7);
        mainFrame.add(mainButton8);
        
        mainFrame.setVisible(true);
    }
}
