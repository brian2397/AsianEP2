/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author brian
 */

import java.io.*;
import java.awt.*;
import javax.swing.*;

import Model.Campo;
import Controller.menuAcoes;

public class InterfaceJogo {
    private JFrame mainFrame;
    private JLabel mainLabel;
    private JPanel mainPanel;
    private JButton mainButton1;
    private JButton power1;
    private JButton power2;
    private JButton power3;
    private JButton power4;
     
    private menuAcoes Listener;

    public InterfaceJogo(int Altura, int Largura, String Data){
        preparaGUI2(Altura, Largura, Data);
    }
    
    private void preparaGUI2(int Altura, int Largura, String Data){
        Listener = new menuAcoes();
        
        mainFrame = new JFrame("Batalha Naval");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(1000,800);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        
        mainLabel = new JLabel("BATALHA NAVAL 2.0");
        mainLabel.setBounds(550, 50, 200, 100);
        
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(Altura,Largura));
        mainPanel.setBounds(400,200,500,400);
        mainPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Battle Field"));
        
        power1 = new JButton();
        power1.setText("Ataca uma posição");
        power1.setBounds(100, 100, 250, 50);
        
        power2 = new JButton();
        power2.setText("Revela uma área 2x2");
        power2.setBounds(100, 150, 250, 50);
        
        power3 = new JButton();
        power3.setText("Ataca uma área 2x2");
        power3.setBounds(100, 200, 250, 50);
        
        power4 = new JButton();
        power4.setText("Ataca uma linha/coluna completa");
        power4.setBounds(100, 250, 250, 50);
        
        mainButton1 = new JButton("Voltar");
        mainButton1.setBounds(800,600,100,50);
        mainButton1.setActionCommand("Voltar");
        mainButton1.addActionListener(Listener);
              
        JButton[] temp = new JButton[Altura*Largura];
        for(int i=0; i<Altura*Largura; i++){
            temp[i] = new JButton();
            mainPanel.add(temp[i]);
        }
     
        mainFrame.add(mainPanel);
        mainFrame.add(mainLabel);
        mainFrame.add(power1);
        mainFrame.add(power2);
        mainFrame.add(power3);
        mainFrame.add(power4);
        mainFrame.add(mainButton1);
        
        mainFrame.setVisible(true);
    }
}
