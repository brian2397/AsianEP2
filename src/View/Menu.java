/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 *
 * @author brian
 */

import java.io.File;
import java.awt.*;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileNameExtensionFilter;

import Model.Campo;
import Controller.menuAcoes;

public class Menu {
    private JFrame mainFrame;
    private JLabel mainLabel;
    private JPanel mainPanel;
    private JFileChooser mainFile;
    private JButton mainButton1;
    private JButton mainButton2;
    private menuAcoes Listener; 
    
    public Menu(){
        preparaGUI();
    }
    
    private void preparaGUI(){
        Listener = new menuAcoes();
        
        mainFrame = new JFrame("Batalha Naval");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(1000,800);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        
        mainLabel = new JLabel("BATALHA NAVAL 2.0");
        mainLabel.setBounds(400, 50, 200, 100);
        
        mainFile = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        Listener.saidas(mainFrame, mainFile);
        
        mainButton1 = new JButton("Abrir arquivo e começar o jogo");
        mainButton1.setBounds(250,200,500,100);
        mainButton1.setActionCommand("Abrir");
        mainButton1.addActionListener(Listener);
        
        mainButton2 = new JButton("Mostrar as intruções");
        mainButton2.setBounds(250,300,500,100);
        mainButton2.setActionCommand("Instrucoes");
        mainButton2.addActionListener(Listener);
    
        mainFrame.add(mainLabel);
        mainFrame.add(mainButton1);
        mainFrame.add(mainButton2);
      
        mainFrame.setVisible(true);
                   
    }
   
    
}
