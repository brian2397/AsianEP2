/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author brian
 */

import java.io.*;

import View.Menu;

public class Campo {
    private int altura;
    private int largura;
    private String data;
    private File arquivo = null;
    private char [][] matriz;
    
    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }
 
    public File getArquivo() {
        return arquivo;
    }

    public void setArquivo(File arquivo) {
        this.arquivo = arquivo;
    }
    
    public char getMatriz(int linha, int coluna) {
        return matriz[linha][coluna];
    }

    public void setMatriz(int linha, int coluna, char value) {
        matriz[linha][coluna] = value;
    }
    
    public String getData(){
        return data;
    }
    
    public void setData(String data){
        this.data = data;
    }
    
    public String Read(File arquivo) {
        String conteudo="";
        try{
            FileReader arq = new FileReader(arquivo);
            BufferedReader br = new BufferedReader(arq);
            String linha="";
            try{
                linha = br.readLine();
                linha = br.readLine();
                String a = linha.substring(0,linha.indexOf(' '));
                String b = linha.substring(linha.indexOf(' ')+1);
                setAltura(Integer.parseInt(a));
                setLargura(Integer.parseInt(b));
                
                matriz = new char[getAltura()][getLargura()];
                
                while(linha!=null){
                    linha=br.readLine();
                    conteudo+=linha;
                }
                
                int k=8;
                for(int i=0; i<getAltura(); i++){
                    for(int j=0; j<getLargura(); j++){
                        setMatriz(i, j, conteudo.charAt(k));
                        k++;
                    }
                }
                //k+30
                String c = conteudo.substring(8,(getAltura()*getLargura())+8);
                setData(c);
                
                arq.close();
            }
            catch(IOException ex){
                conteudo ="Erro: Não foi possivel abrir o arquivo!";
            }
        }
        catch(FileNotFoundException ex){
            conteudo = "Erro: Arquivo não encontrado!";
        }
        
        if(conteudo.contains("Erro"))
            return "";
        else
            return conteudo;
    }
    
}
 